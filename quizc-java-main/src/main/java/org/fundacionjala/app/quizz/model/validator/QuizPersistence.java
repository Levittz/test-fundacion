package org.fundacionjala.app.quizz.model.validator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.stream.JsonReader;
import org.fundacionjala.app.quizz.model.Quiz;
import org.fundacionjala.app.quizz.model.QuizAnswers;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class QuizPersistence {

    public QuizAnswers readJsonFile() {
        Gson gson = new Gson();
        QuizAnswers quizAnswers = null;
        try (JsonReader reader = new JsonReader(new FileReader("myQuiz.json"))) {
            quizAnswers = gson.fromJson(reader, QuizAnswers.class);
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        return quizAnswers;
    }

    public void writeJsonFile(QuizAnswers quizAnswers) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try {
            Writer writer = new FileWriter("myQuiz.json");
            gson.toJson(quizAnswers, writer);
            writer.close();
        } catch (JsonIOException | IOException exception) {
            exception.printStackTrace();
        }
    }
}
