package org.fundacionjala.app.quizz.model.validator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class ValidatorIT {

    @Test
    public void testDateValidator() {
        List<String> errors = new ArrayList<>();
        Validator dateValidator = ValidatorType.DATE.getValidator();

        dateValidator.validate("24/12/2021", null, errors);

        Assert.assertEquals(DateValidator.class, dateValidator.getClass());
        Assert.assertTrue(errors.isEmpty());
    }

    @Test
    public void testUpperCaseValidator() {
        List<String> errors = new ArrayList<>();
        Validator upperCaseValidator = ValidatorType.UPPER_CASE.getValidator();

        upperCaseValidator.validate("QUESTION", null, errors);

        Assert.assertEquals(UpperCaseValidator.class, upperCaseValidator.getClass());
        Assert.assertFalse(errors.isEmpty());
    }

    @Test
    public void testMaxLengthValidator() {
        List<String> errors = new ArrayList<>();
        Validator MaxLengthValidator = ValidatorType.MAX_LENGTH.getValidator();

        MaxLengthValidator.validate("QUESTION", "8", errors);

        Assert.assertEquals(MaxLengthValidator.class, MaxLengthValidator.getClass());
        Assert.assertTrue(errors.isEmpty());
    }

    @Test
    public void testMinLengthValidator() {
        List<String> errors = new ArrayList<>();
        Validator MinLengthValidator = ValidatorType.MIN_LENGTH.getValidator();

        MinLengthValidator.validate("QUESTION", "0", errors);

        Assert.assertEquals(MinLengthValidator.class, MinLengthValidator.getClass());
        Assert.assertTrue(errors.isEmpty());
    }

    @Test
    public void testRequiredValidator() {
        List<String> errors = new ArrayList<>();
        Validator RequiredValidator = ValidatorType.REQUIRED.getValidator();

        RequiredValidator.validate("", "0", errors);
        Assert.assertFalse(errors.isEmpty());
    }

    @Test
    public void testMinValidator() {
        List<String> errors = new ArrayList<>();
        Validator MinValidator = ValidatorType.MIN.getValidator();

        MinValidator.validate("132", "2", errors);

        Assert.assertEquals(MinValidator.class, MinValidator.getClass());
        Assert.assertTrue(errors.isEmpty());
    }

    @Test
    public void testMaxValidator() {
        List<String> errors = new ArrayList<>();
        Validator MaxValidator = ValidatorType.MAX.getValidator();

        MaxValidator.validate("126548654", "5", errors);

        Assert.assertEquals(MaxValidator.class, MaxValidator.getClass());
        Assert.assertFalse(errors.isEmpty());
    }
}
