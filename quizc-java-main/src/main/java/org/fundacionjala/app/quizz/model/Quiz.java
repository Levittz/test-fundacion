package org.fundacionjala.app.quizz.model;

import org.fundacionjala.app.quizz.model.validator.QuizPersistence;

import java.util.ArrayList;
import java.util.List;

public class Quiz {
    private final String title;
    private final List<Question> questions;
    private final QuizPersistence quizPersistence;


    public Quiz(String title) {
        this.title = title;
        this.questions = new ArrayList<>();
        this.quizPersistence = new QuizPersistence();
    }

    public String getTitle() {
        return title;
    }

    public void addQuestion(Question question) {
        questions.add(question);
    }

    public List<Question> getQuestions() {
        return questions;
    }


}
