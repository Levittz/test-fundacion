package org.fundacionjala.app.quizz.model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class QuizAnswers {
    private final Quiz quiz;
    private final UUID id;
    private final List<Answer> answers;

    public QuizAnswers(Quiz quiz) {
        this.quiz = quiz;
        this.id = UUID.randomUUID();
        this.answers = new ArrayList<>();
    }

    public void addAnswer(Answer answer) {
        answers.add(answer);
    }

    public UUID getId() {
        return id;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    @Override
    public String toString() {
       StringBuilder sb = new StringBuilder("* Quiz Title: \n");
       sb.append(quiz.getTitle()+"\n");
       sb.append("=============================== \n");
       sb.append("*Questions: \n");
        for(Object questions: quiz.getQuestions().toArray()){
            sb.append(questions);
        }
       sb.append(quiz.getQuestions()+ "\n");
      sb.append("*Answers: \n");
       for(Object answers: answers.toArray()){
           sb.append(answers + "\n");
       }
       sb.append("\n ========================");

       return sb.toString();

    }
}
