package org.fundacionjala.app.quizz.model.validator;

import org.fundacionjala.app.quizz.model.configuration.QuestionConfiguration;

import java.util.List;
import java.util.Locale;

public class UpperCaseValidator implements Validator {
    private static final String ERROR_MESSAGE = "The value length must be more than ";

    @Override
    public void validate(String value, String conditionValueString, List<String> errors) {
        try {
            String conditionValue = value.toUpperCase(Locale.ROOT);

            if (conditionValueString !=  conditionValue) {
                errors.add(ERROR_MESSAGE + conditionValue);
            }
        } catch (NumberFormatException exception) {
            exception.printStackTrace();
            errors.add(IntegerParser.ERROR_MESSAGE_INVALID_NUMBER);
        }

    }
}
